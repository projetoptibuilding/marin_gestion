<!-- Le code suivant permet de copier les matériau de la table 'articles' et de ne modifier que l'attribut 'id_scenario'
afin de créer un nouveau scénario identique au premier
Je n'ai créé aucun lien pour se rendre sur cette page, il faudrait un bouton sur "Copie" sur la page de gestion des scenarios-->
<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
        
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                       array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}


// On suppose que tous les scenarios relatifs au projet sont stockés dans une table appelée 'scenarios', on trouve le dernier scenario pour la piece concernée
    $req1=$bdd->query('SELECT id_scenario FROM scenarios ORDER BY id_scenario DESC');
    $sce=$req1->fetch();
    $new_scenario=$sce['id_scenario']+1;
        
// Il faut d'abord inscrire le nouveau scenario dans la table 'scenarios'. Je suppose que l'id_scenario EST LA CLE PRIMAIRE (à voir)
    $req3=$bdd->prepare('INSERT INTO scenarios(id_scenario, id_piece)
                        VALUES (:id_scenario, :id_piece)'); //Ajouter les attributs nécessaires
    $req3->execute(array('id_scenario'=>htmlentities($new_scenario), 'id_piece'=>htmlentities($_GET['piece'])));
    
// On sélectionne et on insert tous les matériaux qui nous intéressent
    $req2=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'"');
 
    while($donnes=$req2->fetch())
    {    
        $request=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, surface,
                               type_materiau, libelle, fabricant, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                                VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :surface,
                                :type_materiau, :libelle, :fabricant, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien, :taux_remplacement)');
        
        $request->execute(array('id_scenario'=>$new_scenario, 'id_piece'=>$id_piece, 'code_article'=>htmlentities($donnes['code_article']),
                            'CUPI_article'=>htmlentities($donnes['CUPI_article']), 'MAJ_article'=>htmlentities($donnes['MAJ_article']),
                            'surface'=>htmlentities($donnes['surface']),
                            'poste'=>htmlentities($donnes['poste']), 'type_materiau'=>htmlentities($donnes['type_materiau']),
                            'libelle'=>htmlentities($donnes['libelle']), 'fabricant'=>htmlentities($donnes['fabricant']),
                            'unite'=>htmlentities($donnes['unite']), 'prix_unitaire'=>htmlentities($donnes['prix_unitaire']),
                            'duree_de_vie'=>htmlentities($donnes['duree_de_vie']),
                            'taux_entretien'=>htmlentities($donnes['taux_entretien']),
                            'taux_remplacement'=>htmlentities($donnes['taux_remplacement'])
                            ));
    }

    
// On entre une ligne dans la table 'resultats'
    $req4=$bdd->prepare('INSERT INTO resultats(id_piece, id_scenario)
                        VALUES(:id_piece, :id_scenario)');
    $req4->execute(array('id_piece'=>htmlentities($_GET['piece']), 'id_scenario'=>$new_scenario));
    
    header('Location:calcul/calcul_cout_global.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$new_scenario.'');
?>