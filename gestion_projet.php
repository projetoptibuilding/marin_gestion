<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="DesignOptibuilding.css"/>
</head>
        
<body>
    <header>
    </header>
    
    <section>
        <h1><span>Bienvenue sur la page OptiBuilding !</span></h1>
        
        <p>Pour créer un nouveau projet, remplissez le formulaire suivant.</p>
<!-- Formulaire de création d'un nouveau projet -->
        <form method='post' action='nouveau_projet.php'>
            <p>
                <label for='name'>Nom du projet</label>
                <input type='text' id='name' name='name' required='required'/></br></br>
                
                <label for='date_delivery'>Date de livraison</label>
                <input type='date' id='date_delivery' name='date_delivery'/></br></br>
                
                <textarea name='adress' rows="3" cols="50" placeholder='Adresse'></textarea></br></br>
                
                <label for='zip_code'>Code Postal</label>
                <input type='number' id='zip_code' name='zip_code'/></br></br>
                
                <textarea name='comment' rows="8" cols="50" placeholder='Commentaires' ></textarea></br></br>
        
                <input class='icones add' type='submit' value='Créer un nouveau projet'>
            </p>
        </form>
        </br></br>
        
        <p>Ci dessous, retrouvez tous les projets en cours :</p>

        <p><table>
            <thead><tr>
                <th>Nom</th>
                <th>Date de création</th>
                <th>Date de livraison</th>
                <th>Date de modification</th>
                <th>Auteur</th>
                <th>Adresse</th>
                <th>Code Postal</th>
                <th>Commentaires</th>
                <th>Avancement</th>
            </tr></thead>
            <tbody>
             
<!-- On se connecte à la base de données contenant la table qui contient elle même tous les projets, puis on affiche tous les entrées de la table
     Il faudra rajouter une condition pour afficher les projets en cours d'avancements WHERE progress=(...)
     Le traitement est effectué sur la page nouveau_projet.php -->                            
<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        $affiche=$bdd->query('SELECT* FROM projets ORDER BY id_project');                
        while($donnes=$affiche->fetch())
{?>
            <tr>
                <td><a href='gestion_scenario.php?dbid=<?php echo($donnes['id_project']); ?> '> <?php echo $donnes['name']; ?></a></td>
                <td><?php echo $donnes['date_creation']; ?></td>               
                <td><?php echo $donnes['date_delivery']; ?></td>
                <td><?php echo $donnes['date_modification']; ?></td>
                <td><?php echo $donnes['id_author']; ?></td>
                <td><?php echo $donnes['adress']; ?></td>
                <td><?php echo $donnes['zip_code']; ?></td>
                <td><?php echo $donnes['comment']; ?></td>
                <td><?php echo $donnes['progress']; ?></td>
            </tr>
<?php } ?>
            </tbody> 
        </table></p> 
    </section>                     
    <footer>
    </footer>
</body>
</html>