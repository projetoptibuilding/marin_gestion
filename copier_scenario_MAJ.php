<!--
Cette fonction permet la création d'un nouveau scénario qui prend en compte toutes les mises à jours effectuées sur les matériaux
dans la table 'materiaux'
-->

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
    
//On se connecte à la BDD du projet en cours
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
    
// On se connecte à la bdd optibuilding car nous voulons accèder à  la table 'materiaux'
    try {$optibuilding= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
    
// On suppose que tous les scenarios relatifs au projet sont stockés dans une table appelée 'scenarios', on trouve le dernier scenario pour la piece concernée
    $req1=$bdd->query('SELECT id_scenario FROM scenarios WHERE id_piece="'.$id_piece.'" ORDER BY id_scenario DESC');
    $sce=$req1->fetch();
    $new_scenario=$sce['id_scenario']+1;
    
// Il faut d'abord inscrire le nouveau scenario dans la table 'scenarios'. Je suppose que l'id_scenario EST LA CLE PRIMAIRE
    $req3=$bdd->prepare('INSERT INTO scenarios(id_scenario, id_piece)
                        VALUES (:id_scenario, :id_piece)'); //Ajouter les attributs nécessaires
    $req3->execute(array('id_scenario'=>$new_scenario,'id_piece'=>htmlentities($_GET['piece'])));
    
//On recherche les articles utilisés par le code_mat et on insère les nouveaux articles (avec les mises à jour éventuelles)
    $req2=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'"');
    
    while($donnes=$req2->fetch())
    {
        $req_mat=$optibuilding->query('SELECT * FROM materiaux WHERE code_mat="'.$donnes['code_article'].'"');
        
        $donnes_mat=$req_mat->fetch();
        
        $request=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, surface,
                                type_materiau, libelle, fabricant, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                                
                                VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :surface,
                                :type_materiau, :libelle, :fabricant, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien,
                                :taux_remplacement)');
       
        $request->execute(array('id_scenario'=>$new_scenario, 'id_piece'=>htmlentities($donnes['id_piece']), 'code_article'=>htmlentities($donnes['code_article']),
                            'CUPI_article'=>htmlentities($donnes_mat['CUPI_mat']), 'MAJ_article'=>htmlentities($donnes_mat['MAJ_mat']),
                            'surface'=>htmlentities($donnes['surface']),
                            'poste'=>htmlentities($donnes_mat['poste_mat']), 'type_materiau'=>htmlentities($donnes_mat['type_mat']),
                            'libelle'=>htmlentities($donnes_mat['libelle_mat']), 'fabricant'=>htmlentities($donnes_mat['fabricant_mat']),
                            'unite'=>htmlentities($donnes_mat['unite_mat']), 'prix_unitaire'=>htmlentities($donnes_mat['prix_unitaire_mat']),
                            'duree_de_vie'=>htmlentities($donnes_mat['duree_de_vie_mat']),
                            'taux_entretien'=>htmlentities($donnes_mat['taux_entretien_mat']),
                            'taux_remplacement'=>htmlentities($donnes_mat['taux_remplacement_mat'])
                            ));
    }
    

    
// On entre une ligne dans la table 'resultats'
    $req4=$bdd->prepare('INSERT INTO resultats(id_piece, id_scenario)
                        VALUES(:id_piece, :id_scenario)');
    $req4->execute(array('id_piece'=>htmlentities($_GET['piece']), 'id_scenario'=>$new_scenario));
    
    header('Location:calcul/calcul_cout_global.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$new_scenario.'');
    ?>