<!-- Cette page traite les infos envoyées par le formulaire de la page gestion_projet.php
 Sur cette page, on se connecte à la bdd optibuilding, on insert le nouveau projet dans la table projet puis on crée une bdd avec un nom normalisé : "projet_[id_project]" 
 de cette manière on est sûr de ne pas avoir deux bdd avec le même nom et on évite les problèmes liés aux caractères spéciaux dans les noms de bdd ou de table.
 On peut retrouver facilement une bdd d'un projet car le nom est normalisé
 
 La page conduit à la page suivante : le gestionnaire de scénarios, on fait passer l'id_project via l'URL pour la page suivante dans l'argument "dbid" (data base id)-->

<?php try{$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
						array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
catch(Exception $e)
{die('Erreur : ' . $e->getMessage());}

	$inserer=$bdd->prepare('INSERT INTO projets(name, date_delivery, adress, zip_code, comment) VALUES(:name, :date_delivery, :adress, :zip_code, :comment)');
    $inserer->execute(array('name'=>htmlentities($_POST['name']), 'date_delivery'=>htmlentities($_POST['date_delivery']),'adress'=>htmlentities($_POST['adress']),
							'zip_code'=>htmlentities($_POST['zip_code']), 'comment'=>htmlentities($_POST['comment'])));
	
	$p=$bdd->query('SELECT * FROM projets ORDER BY id_project DESC');
	$project=$p->fetch();
	$project_name='projet_'.$project['id_project'];
	
try{ $new_bdd= new PDO ('mysql:host=localhost;charset=utf8', 'root', '',
						array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $new=$new_bdd->query('CREATE DATABASE IF NOT EXISTS '.$project_name.'');}
	catch(Exception $e)
	{die('Erreur : ' . $e->getMessage());}
	
    header('Location:gestion_scenario.php?dbid='.$project['id_project'].'');
?>