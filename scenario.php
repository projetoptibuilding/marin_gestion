<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
        <header>
        </header>
        <section>
<!-- On récupère l'id du projet et du scénario via l'URL, on se connecte aux bases de données pour retrouver leur nom et les utiliser dans la page -->            
<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
            
    $p=$bdd->query('SELECT * FROM projets WHERE id_project='.htmlentities($_GET['dbid'].''));
    $project=$p->fetch();
            
    $p2=$bdd->query('SELECT* FROM scenarios WHERE id_scenario='.htmlentities($_GET['sce'].''));
    $scenario=$p2->fetch();
?>            
            
            <h1>Page du Projet <?php echo$project['name']; ?></h1>
            <h2>Le scénario : <?php echo$scenario['name']; ?></h2>
            </br></br>
            <a href="gestion_scenario.php?dbid=<?php echo htmlentities($_GET['dbid']); ?>"><input type=button value='Retour aux scénarios'/></a>
        </section>                     
        <footer>
        </footer>
</body>
</html>