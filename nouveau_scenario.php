<!-- Cette page traite le formulaire de la page gestion_scenario pour créer un nouveau scenario
	 On récupère l'id_project associé à ce nouveau scénario vie l'URL et l'argument dbid -->

<?php	$db_name='projet_'.htmlentities($_GET['dbid']);
/* On se connecte à la table scenarios pour y inscrire la nouvelle entrée  */
	try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
				array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
	catch (Exception $e)
	{die('Erreur : ' . $e->getMessage());}
	
	$inserer=$bdd->prepare('INSERT INTO scenarios(name, id_project, comment)
					   VALUES(:name, :id_project,:comment)');
	$inserer->execute(array('name'=>htmlentities($_POST['name']), 'id_project'=>htmlentities($_GET['dbid']), 'comment'=>htmlentities($_POST['comment'])));
	
/*  On récupère l'id_scenario pour donner un nom à la nouvelle table dans la bdd du projet, cette table est celle du scenario
	Le nom du scénario est normalisé : "scenario_[id_scenario]" de cette manière on a pas deux tables qui ont le même nom et on peut les retrouver facilement*/	
	$p=$bdd->query('SELECT * FROM scenarios ORDER BY id_scenario DESC');
	$scenario=$p->fetch();
	$scenario_name='scenario_'.$scenario['id_scenario'];

/* On se connecte à la bdd du projet pour y créer la table correspondant au scénario que l'on crée */	
	try {$bdd= new PDO ('mysql:host=localhost;dbname='.$db_name.';charset=utf8', 'root', '',
						   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
	catch (Exception $e)
	{die('Erreur : ' . $e->getMessage());}

	$req=$bdd->query('CREATE TABLE '.$scenario_name.'
					(id INT(6) AUTO_INCREMENT PRIMARY KEY)');

/* On dirige vers la page du scnéario en transmettant les arguments via l'URL */					
	header('Location:scenario.php?dbid='.htmlentities($_GET['dbid']).'&sce='.$scenario['id_scenario'].'');
?>
