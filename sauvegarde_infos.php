<!--
Cette page permet la sauvegarde des infos concernant le projet en cours. Ces données sont stockées dans la table 'informations'
-->

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
        
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                       array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
    
    // IL faut au préalable que l'on insère une ligne en créant le scénario

    $req=$bdd->prepare('UPDATE informations
                       SET taux_inflation=:taux_inflation, duree_exploitation=:duree_exploitation,
                       cout_ext_fixe=:cout_ext_fixe, cout_ext_annuel=:cout_ext_annuel, comment=:comment, cout_externalite=:cout_externalite,
                       cout_externalite_courant=:cout_externalite_courant');
    //modifier l'id du scenario et de la piece dans la requête suivante                   
    $req->execute(array('taux_inflation'=>htmlentities($_POST['taux_inflation']),
                        'duree_exploitation'=>htmlentities($_POST['duree_exploitation']),'cout_ext_fixe'=>htmlentities($_POST['cout_ext_fixe']),
                        'cout_ext_annuel'=>htmlentities($_POST['cout_ext_annuel']), 'comment'=>htmlentities($_POST['comment']), 'cout_externalite'=>htmlentities($_POST['cout_externalite']),
                        'cout_externalite_courant'=>htmlentities($_POST['cout_externalite_courant']),
                        ));              
    
    header('Location:infos_projet.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$id_scenario.' ');
?>