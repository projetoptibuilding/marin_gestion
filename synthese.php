<!--
Cette page reçoit des informations du formulaire de la page 'comparaison.php' et génère une synthèse des scnéarios choisis
-->

<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
?>
<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="#"/>
</head>
        
<body>
<!--
    Problème ici, pour récupérer les résultats par un POST, il faut savoir les noms des données qui nous intéressent, il faut donc
    savoir le nombre de pièces.
    On compte le nombre de pièces, on sait combien de données on va récup et on peut alors savoir exactement quels scénarios ont été séléctionnés
    
    Ou alors, on ajoute un attribut dans la table 'scenarios', attribut 'selection' si la valeur est 'oui', on l'affiche. Du coup le formulaire envoit
    sur un code qui permet de mettre à jour les données relatives à la séléction.
-->
    
</body>
</html>