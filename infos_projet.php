<!--
Cette page présente un formulaire pour rensigner des informations relatives au projet en cours : la durée d'exploitaiton, le taux d'inflation
et les externalités.
-->

<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="DesignOptibuilding.css"/>
</head>
        
<body>
    <?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $piece="piece_".$id_piece;
        $id_scenario=htmlentities($_GET['scenario']);
        $scenario="scenario_".$id_scenario;
    
        if(isset($_POST['taux_inflation']) && isset($_POST['duree_exploitation']) &&
          isset($_POST['autre_annuel']) && isset($_POST['autre']) && isset($_POST['comment']))
            {
            require("calcul/fonctions_cout_global.php");
            
            $duree_exp=htmlentities($_POST['duree_exploitation']);
            $autre=htmlentities($_POST['autre']);
            $autre_annuel=htmlentities($_POST['autre_annuel']);
            $comment=htmlentities($_POST['comment']);
            $taux_inflation=htmlentities($_POST['taux_inflation']);
            $coeff_inflation=1+$taux_inflation/100;
            $coeff_nul=1;    
            
            $externalities=sprintf('%.2f', (externality_cost($duree_exp,$coeff_nul,$autre_annuel)+$autre));
            $externalities_courant=sprintf('%.2f', (externality_cost($duree_exp,$coeff_inflation,$autre_annuel)+$autre));
        
            }else{
            try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
            catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
            $req=$bdd->query('SELECT * FROM informations');
            $donnes=$req->fetch();
            
            $taux_inflation=$donnes['taux_inflation'];
            $duree_exp=$donnes['duree_exploitation'];
            $autre=$donnes['cout_ext_fixe'];
            $autre_annuel=$donnes['cout_ext_annuel'];
            $comment=$donnes['comment'];
            $externalities=$donnes['cout_externalite'];
            $externalities_courant=$donnes['cout_externalite_courant'];
            }
        
    ?>
    
    <p><a href='calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
     <button class='icones left_arrow'>  Retour au scénario</button></a></p>
    
    <p>
        <form method="post" action="">
        
        <fieldset>
            <legend>Taux</legend>
            <label for='taux_inflation'>Taux d'inflation </label>
            <input type='number' id='taux_inflation' name='taux_inflation' value='<?php echo $taux_inflation;?>' step="0.001" style="width:50px;"/> %</br></br>
            
            <label for='duree_exploitation'>Durée d'exploitation </label>
            <input type='number' id='duree_exploitation' name='duree_exploitation' value='<?php echo $duree_exp;?>' min="0" step="0.1" style="width:50px;"/> ans
        </fieldset>
        
        <fieldset>
            <legend>Externalités</legend>
            
            <label for='autre'>Coûts fixes </label>
            <input type='number' id='autre' name='autre' value='<?php echo $autre;?>' step='0.01' style="width:80px;"/> €</br></br>
            
            <label for='autre_annuel'>Coûts annuels </label>
            <input type='number' id='autre_annuel' name='autre_annuel' value='<?php echo $autre_annuel;?>' step='0.01' style="width:80px;"/> €</br></br>
            
            <textarea name='comment' rows='3' cols='50' placeholder='commentaires'/><?php echo $comment; ?></textarea></br></br>
            
            Coût des externalités : <?php echo number_format($externalities,2,"."," ");?> €</br>
            Coût courant des externalités : <?php echo number_format($externalities_courant,2,"."," ");?> €</br>      
        </fieldset>
        
        <p><button type='submit' class='icones calculer'> Calculer </button></p>
        </form></p>
        
        <p><form method="post" action="sauvegarde_infos.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>">        
            <input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>' />
            <input type='hidden' name='duree_exploitation' value='<?php echo $duree_exp;?>'/>
            <input type='hidden' name='cout_ext_fixe' value='<?php echo $autre;?>'/>
            <input type='hidden' name='cout_ext_annuel' value='<?php echo $autre_annuel;?>'/>
            <input type='hidden' name='comment' value='<?php echo $comment;?>'/>
            <input type='hidden' name='cout_externalite' value='<?php echo $externalities;?>'/>
            <input type='hidden' name='cout_externalite_courant' value='<?php echo $externalities_courant;?>'/>
            
            <button type='submit' class='icones save'> Enregistrer</button>
        </form>
     </p>

</body>
</html>