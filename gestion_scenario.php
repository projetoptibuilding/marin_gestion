<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="DesignOptibuilding.css"/>
    </head>
        
    <body>
        <header>
        </header>
        <section>
  
<!-- On se connecte à la bdd optibuilding et la table projets pour retrouver le nom du projet sur lequel on travaille.
Pour cela on récupère l'id_project via l'URL -->            
<?php   $dbid=htmlentities($_GET['dbid']);
        try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
                
        $affiche=$bdd->query('SELECT * FROM projets WHERE id_project='.$dbid.'');
        $donnes=$affiche->fetch();
        $dbname=$donnes['name'];
?>
        
            <h1><span>Page du Projet <?php echo($dbname); ?></span></h1>
<!-- Un formulaire pour créer des scénarios, le traitement est fait sur la page nouveau_scenario.php -->            
            <p>Pour créer un nouveau scénario, remplissez le formulaire suivant.</p>
            <form method='post' action='nouveau_scenario.php?dbid=<?php echo($dbid); ?>'>
            <p>
                <label for='name'>Nom du Scénario</label>
                <input type='text' id='name' name='name' required='required'/></br></br>
                        
                <textarea name ='comment' placeholder='Commentaires'></textarea></br></br>
                
                <button class='icones add' type='submit'>  Créer un nouveau scénario</button>
            </p>
            </form>
                
            <p>Ci dessous vous trouverez les différents scénarios pour ce projet.</p>
<!-- On se connecte à la table qui contient tous les scénarios (dans la bdd optibuilding) et on affiche les scénarios disponibles pour ce projet donné-->
<?php   try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            { die('Erreur : ' . $e->getMessage());}
                    
        $affiche=$bdd->query('SELECT* FROM scenarios WHERE id_project='.$dbid.' ORDER BY id_scenario DESC');
            
        while($donnes=$affiche->fetch())
        {
?>
            <p><strong>Scénario</strong>
                <a href="scenario.php?dbid=<?php echo($dbid); ?>&sce=<?php echo($donnes['id_scenario']);?>"><?php echo ($donnes['name']); ?></a>
                (Création <?php echo $donnes['date_creation']; ?>)
<?php   };
?>
            </p></br></br>
            <a href="gestion_projet.php"><input type='button' class='icones left_arrow' value='Retour aux projets'></a>
            
        </section>                     
        <footer>
        </footer>
</body>
</html>