<!--
Cette page contient des compléments de code, ce sont surtout des fonctions à implémenter sur des pages existantes
-->

<?php
// Cette fonction est une fonction de suppression de scénario,insérer un bouton dans la page de gestion des scénarios

/*
!!! ATTENTION !!! Je considère que des index ont été créés dans les tables pour permettre la suppresion de toutes les entrées concernées
dans les différentes tables qui constituent la base de données
*/
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_scenario=htmlentities($_GET['scenario']);
    
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
        
    $req=$bdd->query('DELETE FROM scenarios WHERE id_scenario="'.$id_scenario.'"');
        
    header('Location:');
    

?>

<?php
// Cette fonction est une fonction de suppression d'une pièce,insérer un bouton dans la page de gestion des pièces
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
        
    $req=$bdd->query('DELETE FROM pieces WHERE id_piece="'.$id_piece.'"');
        
    header('Location:');
    

?>