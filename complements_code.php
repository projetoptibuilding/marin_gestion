<!--
Cette page contient des codes qui doivent être intégrés dans les pages déjà codées, notamment pour la gestion des scénarios/pièces/scénarios
Les informations sont les mêmes c'est à dire que l'on utilise que les id car c'est la seule info dont on est sûr et elle est imposée par le code
donc il n'y a pas de risque que la données soit dans le mauvais format (caractère spécial, espace...)

!!! Je n'ai pas encore tester ce code, il contient surement des erreurs!!!
-->


<!--
// Code à intégrer lors de la création d'une base de données pour un nouveau projet.
//Le nom de la base de données est contenu dans $projet
//L'id d'une piece est contenu dans $id_piece
//L'id d'un scénario est contenu dans $id_scenario

// On créé trois tables : 'articles' est la table qui contient tous les articles utilisés pour le projet, ils sont classés selon leur piece/scenario 
//                       'resultats' stocke les résultats des calculs que l'on a effectués. Classés par le couple piece/scenario et toutes les infos calculées
//						 'informations' stocke les infos concernant le projet en cours
-->
<?php
	$id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;	
	
	try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
						   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
	catch (Exception $e)
	{die('Erreur : ' . $e->getMessage());}

	$req1=$bdd->query('CREATE TABLE articles
					( 
					id_article INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    id_scenario INT(11) UNSIGNED,
                    id_piece INT(11) UNSIGNED,
                    code_article VARCHAR(7),
					MAJ_article DATE,
					CUPI_article VARCHAR(3),
                    surface FLOAT UNSIGNED,
                    poste VARCHAR(15),
                    type_materiau VARCHAR(25),
                    libelle TEXT,
                    fabricant VARCHAR(50),
                    unite VARCHAR(5),
                    prix_unitaire FLOAT UNSIGNED,
                    duree_de_vie FLOAT UNSIGNED,
                    taux_entretien FLOAT UNSIGNED,
                    taux_remplacement FLOAT UNSIGNED
					)
                    ENGINE=INNODB');
    //id_scenario et id_piece sont des clés étrangères
    
    $req2=$bdd->query('CREATE TABLE resultats
					(
					id_resultat INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					id_scenario INT(11) UNSIGNED,
					id_piece INT(11) UNSIGNED,
					cout_construction DOUBLE UNSIGNED,
					cout_remplacement DOUBLE UNSIGNED,
					cout_maintenance DOUBLE UNSIGNED,
					cout_global DOUBLE UNSIGNED,
					cout_remplacement_courant DOUBLE UNSIGNED,
					cout_maintenance_courant DOUBLE UNSIGNED,
					cout_global_courant DOUBLE UNSIGNED
					)
					ENGINE=INNODB');
    //id_scenario et id_piece sont des clés étrangères
	
	$req3=$bdd->query('CREATE TABLE informations
					(
					id_information INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					taux_inflation FLOAT,
					duree_exploitation FLOAT UNSIGNED,
					cout_ext_fixe DOUBLE,
					cout_ext_annuel DOUBLE,
					comment TEXT,
					cout_externalite DOUBLE UNSIGNED,                                           
					cout_externalite_courant DOUBLE UNSIGNED
					)             
					ENGINE=INNODB');
				
	//La table 'informations' n'aura qu'une seule entrée que l'on modifiera à chaque enregistrement.
	//A la création de la table, on entre une ligne vide, il suffit d'entrer un seul attribut avec la valeur 0, $nul=0
	$nul=0;
	$req4=$bdd->prepare('INSERT INTO informations(taux_inflation) VALUES(:taux_inflation)');
    $req4->execute(array('taux_inflation'=>$nul));
	
//Il faut aussi envisager la création d'une table contenant toutes les pieces (table 'piece')
//et une autre contenant tous les scénarios (table 'scenarios')
	$req5=$bdd->query('CREATE TABLE pieces
					(
					id_piece INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					nom_piece VARCHAR(50),
					comment_piece TEXT
					)
					ENGINE=INNODB');
	
	$req6=$bdd->query('CREATE TABLE scenarios
					(
					id_scenario INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					id_piece INT(11) UNSIGNED,
					nom_scenario VARCHAR(50),
					comment_scenario TEXT
					)
					ENGINE=INNODB');
	
	$req11=$bdd->query('ALTER TABLE articles
						ADD CONSTRAINT fk_article_scenario
						FOREIGN KEY (id_scenario)
						REFERENCES '.$projet.'.scenarios(id_scenario)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req12=$bdd->query('ALTER TABLE articles
						ADD CONSTRAINT fk_article_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$projet.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req21=$bdd->query('ALTER TABLE resultats
						ADD CONSTRAINT fk_resultat_scenario
						FOREIGN KEY (id_scenario)
						REFERENCES '.$projet.'.scenarios(id_scenario)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req22=$bdd->query('ALTER TABLE resultats
						ADD CONSTRAINT fk_resultat_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$projet.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req61=$bdd->query('ALTER TABLE scenarios
						ADD CONSTRAINT fk_scenario_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$projet.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');

?>


<!-- Le code suivant n'a pas été testé mais il ne devrait pas y avoir trop de problème. La création d'une pièce et d'un scénario est
préalable. Ce code doit être inséré avant toute manipulation sur un scénario car il y a des contraintes imposées par les index,
si les contraintes ne sont pas respectées, on ne peut rien enregistrer -->

<!-- Une fois que l'on a créé une piece, on inscrit cette création dans la table 'pieces'.
Pas besoin de créer une table au nom de la pièce ou du scénario -->
<?php


    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
      
    $req=$bdd->prepare('INSERT INTO pieces(nom_piece, comment_piece)
                        VALUES(:nom_piece, :comment_piece)');
    $req->execute(array('nom_piece'=>$nom_piece,'comment_piece'=>$comment_piece));
    //Avec $id_piece et $comment_piece que l'on obtient par un formulaire
?>

<!-- Une fois que l'on a créé un scenario, on inscrit cette création dans la table 'scenarios' et on créé une entrée dans résultats-->
<?php
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
      
    $req=$bdd->prepare('INSERT INTO scenarios(id_piece, nom_scenario, comment_scenario)
                        VALUES(:id_piece, :nom_scenario, :comment_scenario)');
    $req->execute(array('id_piece'=>$id_piece, 'nom_scenario'=>$nom_scenario,'comment_scenario'=>$comment_scenario)));
    //Avec $id_scenario et $comment_scenario que l'on obtient par un formulaire
    // et $id_piece qui correspond à la piece que l'on étudie, on trouve sa valeur par une requête dans la table 'pieces'
	
	//A la création d'un scénario, on insère une ligne dans la table 'resultats', cette ligne sera mise à jour ensuite.
	$req2=$bdd->prepare('INSERT INTO resultats(id_piece, id_scenario)
                        VALUES(:id_piece, :id_scenario)');
    $req2->execute(array('id_piece'=>$id_piece, 'id_scenario'=>$id_scenario));
	
?>
