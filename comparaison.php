<!--
Cette page permet la comparaison des scénarios
-->
<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
?>
<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="DesignOptibuilding.css"/>
</head>
        
<body> 
    <?php
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
        
        $req=$bdd->query('SELECT * FROM informations');
        $donnes=$req->fetch();
        
        $taux_inflation=$donnes['taux_inflation'];
        $duree_exp=$donnes['duree_exploitation'];
        $autre=$donnes['cout_ext_fixe'];
        $autre_annuel=$donnes['cout_ext_annuel'];
        $externalities=$donnes['cout_externalite'];
        $externalities_courant=$donnes['cout_externalite_courant'];
    ?>
    
     <p><fieldset>Informations sur le projet
        <fieldset>
            <legend>Taux</legend>
            Taux d'inflation : <?php if(isset($taux_inflation)){echo number_format($taux_inflation,2,"."," ");}else{echo 2.0;}?> %</br>
            Durée d'exploitation : <?php if(isset($duree_exp)){echo $duree_exp;}else{echo 0;}?> ans</br>
        </fieldset>
        
        <fieldset>
            <legend>Externalités</legend>
            Coûts fixes : <?php if(isset($autre)){echo number_format($autre,2,"."," ");}else{echo 0;}?> €</br>
            Coûts annuels : <?php if(isset($autre_annuel)){echo number_format($autre_annuel,2,"."," ");}else{echo 0;}?> €</br>
            </br>
            Coût des externalités : <?php echo number_format($externalities,2,"."," ");?> €</br>
            Coût courant des externalités : <?php echo number_format($externalities_courant,2,"."," ");?> €</br>
        </fieldset>
        </fieldset></p>
     
    <p>Sélectionnez les scénarios et faites la synthèse de votre projet. Le coût global affiché prend en compte les externalités.</p>
    
    <p><form method='post' action=''>
    
    <?php                
    $affiche_piece=$bdd->query('SELECT* FROM pieces ORDER BY id_piece');
    while($donnes_piece=$affiche_piece->fetch()){
        ?>
        <p><table>
            <caption><?php echo $donnes_piece['nom'];?></caption>
            <thead>
                <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th colspan=3>Coût brut</th>
                        <th colspan=3>Coût courant</th>
                </tr>
                <tr>
                        <th>Scénario</th>
                        <th>Commentaires</th>
                        <th>Construction</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                </tr>
            </thead>
        <?php
//Je voudrais faire une jointure de table mais ça ne marche pas bien donc je fais deux requêtes mais c'est pas top niveau code
        $affiche_scenario=$bdd->query('SELECT* FROM scenarios WHERE id_piece="'.$donnes_piece['id_piece'].'" ORDER BY id_scenario');
        while($donnes_scenario=$affiche_scenario->fetch())
        {    
        $affiche_resultat=$bdd->query('SELECT* FROM resultats WHERE id_scenario="'.$donnes_scenario['id_scenario'].'" ');
        $donnes_resultat=$affiche_resultat->fetch();
        
        ?>
        <tr>
            <td>
                <input type='radio' name='<?php echo $donnes_piece['id_piece'];?>' value='<?php echo $donnes_scenario['id_scenario'];?>'
                <?php if(isset($_POST[$donnes_piece['id_piece']]) && $_POST[$donnes_piece['id_piece']]==$donnes_scenario['id_scenario']){ ?> checked='checked' <?php } ?>'/>
                <a href='calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $donnes_piece['id_piece'];?>&scenario=<?php echo $donnes_scenario['id_scenario'];?>'>
            Scenario <?php echo $donnes_scenario['id_scenario']; ?></a>
            </td>
            <td><?php echo $donnes_scenario['comment']; ?></td>
            <td><?php echo number_format($donnes_resultat['cout_construction'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnes_resultat['cout_remplacement'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnes_resultat['cout_maintenance'],2,"."," "); ?> €</td>            
            <td><?php echo number_format($donnes_resultat['cout_global'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnes_resultat['cout_remplacement_courant'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnes_resultat['cout_maintenance_courant'],2,"."," "); ?> €</td>            
            <td><?php echo number_format($donnes_resultat['cout_global_courant'],2,"."," "); ?> €</td>
        </tr>
        <?php
        }
        ?>
        </table></p>
    <?php
    }
    ?>
    <input type='submit' value='Synthèse'/>
    </form></p>
    
    <p><table>
    <caption>Synthèse du projet</caption>
        <thead>
                <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th colspan=3>Coût brut</th>
                        <th colspan=3>Coût courant</th>
                </tr>
                <tr>
                        <th></th>
                        <th>Scénario</th>
                        <th>Construction</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                </tr>
        </thead>
        
        <tbody>
                <?php
// On commence par définir des variables qui permettent de calculer les coûts.
// Les valeurs de ces variables augmentent à chaque itération
// On prend en compte les externalités dans le cout global
                        $total_construction=0;
                        $total_remplacement=0;
                        $total_maintenance=0;
                        $total_global=$externalities;
                        $total_remplacement_courant=0;
                        $total_maintenance_courant=0;
                        $total_global_courant=$externalities_courant;
                
                        $affiche_piece=$bdd->query('SELECT* FROM pieces ORDER BY id_piece');
                        while($donnes_piece=$affiche_piece->fetch()){ ?>
                <tr>
                <?php   
// La boucle if sert à n'afficher les lignes du tableau seulement si des cases ont été sélectionnées dans le formulaire
                        if (isset($_POST[$donnes_piece['id_piece']])) {
                                $id_scenario=$_POST[$donnes_piece['id_piece']];
                
                $affiche_resultat=$bdd->query('SELECT* FROM resultats WHERE id_scenario="'.$id_scenario.'" ');
                $donnes_resultat=$affiche_resultat->fetch();
                ?>
                        <td><?php echo $donnes_piece['nom'];?></td>
                        <td><?php echo $_POST[$donnes_piece['id_piece']];?></td>
                        <td><?php $total_construction+=$donnes_resultat['cout_construction'];
                                echo number_format($donnes_resultat['cout_construction'],2,"."," "); ?> €</td>
                        <td><?php $total_remplacement+=$donnes_resultat['cout_remplacement'];
                                echo number_format($donnes_resultat['cout_remplacement'],2,"."," "); ?> €</td>
                        <td><?php $total_maintenance+=$donnes_resultat['cout_maintenance'];
                                echo number_format($donnes_resultat['cout_maintenance'],2,"."," "); ?> €</td>
                        <td><?php $total_global+=$donnes_resultat['cout_global'];
                                echo number_format($donnes_resultat['cout_global'],2,"."," "); ?> €</td>
                        <td><?php $total_remplacement_courant+=$donnes_resultat['cout_remplacement_courant'];
                                echo number_format($donnes_resultat['cout_remplacement_courant'],2,"."," "); ?> €</td>
                        <td><?php $total_maintenance_courant+=$donnes_resultat['cout_maintenance_courant'];
                                echo number_format($donnes_resultat['cout_maintenance_courant'],2,"."," "); ?> €</td>
                        <td><?php $total_global_courant+=$donnes_resultat['cout_global_courant'];
                                echo number_format($donnes_resultat['cout_global_courant'],2,"."," "); ?> €</td>
                </tr>                
                <?php
                }else{
// Si la condition n'est pas vérifiée, on renvoit une ligne vide
                ?>
                        <td><?php echo $donnes_piece['nom'];?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                <?php
                        }
                        }?>
                <tr>
                        <td>Total</td>
                        <td></td>
                        <td><?php if(isset($total_construction)){echo number_format($total_construction,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_remplacement)){echo number_format($total_remplacement,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_maintenance)){echo number_format($total_maintenance,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_global)){echo number_format($total_global,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_remplacement_courant)){echo number_format($total_remplacement_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_maintenance_courant)){echo number_format($total_maintenance_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_global_courant)){echo number_format($total_global_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                </tr>
        </tbody>
    </table></p>
</body>
</html>